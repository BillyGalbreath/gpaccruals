package net.pl3x.bukkit.gpaccruals.listener;

import me.ryanhamshire.GriefPrevention.GriefPrevention;
import me.ryanhamshire.GriefPrevention.events.AccrueClaimBlocksEvent;
import net.pl3x.bukkit.gpaccruals.Helper;
import net.pl3x.bukkit.gpaccruals.Logger;
import net.pl3x.bukkit.gpaccruals.configuration.Config;
import net.pl3x.bukkit.gpaccruals.hook.GPHook;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class GPListener implements Listener {
    @EventHandler
    public void onClaimAccrual(AccrueClaimBlocksEvent event) {
        int incrementPerHour = Helper.getMaxValue(Config.INCREMENTS, "increment", event.getPlayer());
        if (incrementPerHour < 0) {
            return; // fallback to default; do nothing
        }

        if (event.isIdle()) {
            incrementPerHour = (int) (incrementPerHour * (GriefPrevention.instance.config_claims_accruedIdlePercent / 100.0D));
        }

        event.setBlocksToAccruePerHour(incrementPerHour);

        Logger.debug("Increment accrual blocks for " +
                event.getPlayer().getName() + " by " +
                event.getBlocksToAccrue() + " (" +
                incrementPerHour + "per hour)");
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        GPHook.setMaxCap(event.getPlayer());
    }
}
