package net.pl3x.bukkit.gpaccruals;

import org.bukkit.entity.Player;

import java.util.Map;

public class Helper {
    public static int getMaxValue(Map<String, Integer> map, String type, Player player) {
        int value = -1;
        for (Map.Entry<String, Integer> entry : map.entrySet()) {
            if (player.hasPermission("griefprevention.accruals." + type + "." + entry.getKey()) && entry.getValue() > value) {
                value = entry.getValue();
            }
        }
        return value;
    }
}
