package net.pl3x.bukkit.gpaccruals.hook;

import me.ryanhamshire.GriefPrevention.GriefPrevention;
import net.pl3x.bukkit.gpaccruals.Helper;
import net.pl3x.bukkit.gpaccruals.Logger;
import net.pl3x.bukkit.gpaccruals.configuration.Config;
import org.bukkit.entity.Player;

public class GPHook {
    public static void setMaxCap(Player player) {
        int maxCap = Helper.getMaxValue(Config.MAXCAPS, "maxcap", player);

        Logger.debug("Set accrual max cap for " + player.getName() + " to " + maxCap);
        GriefPrevention.instance.dataStore
                .getPlayerData(player.getUniqueId())
                .setAccruedClaimBlocksLimit(maxCap);
    }
}
