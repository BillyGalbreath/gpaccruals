package net.pl3x.bukkit.gpaccruals;

import net.pl3x.bukkit.gpaccruals.command.CmdGPAccruals;
import net.pl3x.bukkit.gpaccruals.configuration.Config;
import net.pl3x.bukkit.gpaccruals.configuration.Lang;
import net.pl3x.bukkit.gpaccruals.hook.GPHook;
import net.pl3x.bukkit.gpaccruals.listener.GPListener;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;

public class GPAccruals extends JavaPlugin {
    @Override
    public void onEnable() {
        Config.reload();
        Lang.reload();

        if (!getServer().getPluginManager().isPluginEnabled("GriefPrevention")) {
            Logger.error("# GriefPrevention NOT found and/or enabled!");
            Logger.error("# This plugin requires GriefPrevention to be installed and enabled!");
            return;
        }

        getServer().getPluginManager().registerEvents(new GPListener(), this);

        getServer().getOnlinePlayers().forEach(GPHook::setMaxCap);

        getCommand("gpaccruals").setExecutor(new CmdGPAccruals(this));

        Logger.info(getName() + " v" + getDescription().getVersion() + " enabled!");
    }

    @Override
    public void onDisable() {
        Logger.info(getName() + " disabled.");
    }

    public static GPAccruals getPlugin() {
        return GPAccruals.getPlugin(GPAccruals.class);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&4" + getName() + " is disabled. See console log for more information."));
        return true;
    }
}
